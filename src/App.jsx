import axios from "axios";
import { UserContextProvider } from "./UserContext";
import Routes from "./Routes";
function App() {
  axios.defaults.baseURL='http://localhost:3999';
  axios.defaults.withCredentials=true;
  // localStorage.setItem('access_token', data.access_token);
  // localStorage.getItem('access_token',data.access_token)
  return (
    <UserContextProvider>
      <Routes/>
    </UserContextProvider>
  )
}

export default App

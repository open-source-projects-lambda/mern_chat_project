import RegisterAndLoginForm from "./RegisterAndLoginForm.jsx";
import {useContext} from "react";
import {UserContext} from "./UserContext.jsx";
import Chat from "./Chat";

export default function Routes() {
  const {username, id} = useContext(UserContext);
  // localStorage.setItem('access_token', data.access_token);
  //   localStorage.getItem('access_token',data.access_token)

  if (username) {
    return <Chat />;
  }

  return (
    <RegisterAndLoginForm />
  );
}